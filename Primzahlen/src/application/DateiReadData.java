package application;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Description: General Purpose Filehandling
 *
 * Two Constructors Project: Primzahlen 
 * Class : DateiReadData
 * java Package: application 
 * Created: 15.03.2018 10:08:54
 *
 * @author Guenter Schmitz
 * @version 1.0.0
 **/
public class DateiReadData {

	private String dateiName;
	private File file;
	private BufferedReader br;

	/**
	 * Constructor 1: erwartet den Filenamen als String
	 * 
	 * @param fileNamen
	 *           Name der Datei
	 */
	public DateiReadData(String dateiName) {
		this.dateiName = dateiName;
		this.file = new File(this.dateiName);
		initDZL();
	}

	/**
	 * Constructor 2: erwartet File Handle
	 * 
	 * @param file
	 *           Instanz des Dateinamens
	 */
	public DateiReadData(File file) {
		this.file = file;
		this.dateiName = this.file.getName();
		initDZL();
	}

	// Bereitet den Bufferreader vor
	private void initDZL() {
		if (!this.file.canRead() || !this.file.isFile()) {
			System.out.println("Datei nicht lesbar: " + file.getName());
			return;
		}

		// here file is ok
		System.out.println("Datei Pfad: " + this.file.getAbsolutePath());
		System.out.println("Datei Name: " + this.dateiName);
	
		try {
			br = new BufferedReader(new FileReader(this.dateiName));
		} catch (FileNotFoundException e) {
			System.out.println("ReadFile Bufferreader fehlgeschlagen");
			e.printStackTrace();
		}
	}
	

	/**
	 * liest eine Zeile aus Datei von Input Stream
	 * 
	 * @return zeile String, if any, otherwise null 		
	 * @author	G. Schmitz
	 * @since	1.0 (15.03.2018)
	 *
	 **/
	public String readOneLine() {
		String zeile;
			try {
				zeile = this.br.readLine();
			} catch (IOException e) {
				zeile = null;
				e.printStackTrace();
			}
			return zeile;
		}
	

	/**
	 * liest ein komplettes String Array aus Datei von Input Stream
	 * 
	 * @return  zeilenArray  Array if any, otherwise null 		
	 * @author	G. Schmitz
	 * @since	1.0 (15.03.2018)
	 *
	 **/
	public ArrayList <String> readAllLines() {
		String zeile;
		ArrayList <String> liste = new ArrayList<String>();
		
		while((zeile = this.readOneLine()) != null) 
			liste.add(zeile);
		return liste;
		}
	
	
	

		
}
