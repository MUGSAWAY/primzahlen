/**
 * 
 */
package application;

import java.util.ArrayList;
import java.util.List;

/**
 * Description:
 * 				wandelt Zahlen (Radix 10, char) in andere Radizes um. 
 * 				unterstützt werden, 2 bis 9 als Ziel Radizes.
 *
 * Project: Primzahlen
 * Class  : RadixChange.java
 * Package: application
 * Created: 14.04.2020 09:11:33
 *
 * @author     Guenter Schmitz
 * @version    1.0.0
 **/
public class RadixChange {
	

	/**
	 * Constructor:
	 */
	public RadixChange() {
		// TODO Auto-generated constructor stub
	}
	ArrayList <ArrayList<String>> radizes = new ArrayList<ArrayList<String>>();
	
	
	// Wandelt pzList zu den Basen 2 bis 9 um
	public ArrayList <ArrayList<String>> getRadixList(ArrayList <String> pzList, int radixcount){

		int i, j, z, number;
		
		// loop ueber alle Radizes
		for (int allRadix=2; allRadix<10;allRadix++){
			ArrayList <String> radixList = new ArrayList <String>();
			
			// loop ueber die ersten "radixCount Primzahlen" 
			for (i=0; i<radixcount;i++){
				char[] buf_reverse = new char[50];
				char[] buf_straight = new char[50];
				
				number = Integer.parseInt(pzList.get(i));
				j=0;
				
				do {
					z = number %allRadix ;
					buf_reverse[j]= Character.forDigit(z, 10);
					number = number/allRadix;
					j++;
				}while (number != 0);
				buf_reverse[j]= '\0';

				
				for (int loop =0; loop<j; loop++){
					buf_straight[loop]=buf_reverse[j-1-loop];
				}
				buf_straight[j]='\0';
				
				radixList.add(new String(buf_straight));
			}
			radizes.add(radixList);	// hang on Liste
		}
		
		return radizes;
	}
	
	
	

}
