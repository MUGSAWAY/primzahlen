package application;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Description:
 *
 * Class : DateiWriteData.java  Created: 17.03.2018
 *
 * @author Guenter Schmitz
 * @version 1.0.0
 **/
public class DateiWriteData {

//	private ArrayList<String> stringPuffer = new ArrayList<String>();
	private String dateiName;
	private File file;

	/**
	 * Constructor 1: erwartet den Filenamen als String
	 * 
	 * @param fileNamen
	 *           Name der Datei
	 */
	public DateiWriteData(String dateiName) {
		this.dateiName = dateiName;
		this.file = new File(this.dateiName);
	}

	/**
	 * Constructor 2: erwartet erwartet File Handle
	 * 
	 * @param file
	 *           Handle
	 */
	public DateiWriteData(File file) {
		this.file = file;
		this.dateiName = this.file.getName();
	}

//	public void writeStringToArray(String toArray, Boolean withEOL) {
//		stringPuffer.add(toArray);
//		if (withEOL)
//			stringPuffer.add(System.getProperty("line.separator"));
//	}

	// writes the stringArray to a File.
	public boolean writeArrayToFile(String[] outToFile, boolean withEOL) {
		{
			try {
//				File file = new File(this.dateiName);

				// BufferedWriter writer give better performance
//				BufferedWriter bw = new BufferedWriter(new FileWriter(file, true));
				BufferedWriter bw = new BufferedWriter(new FileWriter(this.file));
				for (int i = 0; i < outToFile.length; i++) {
					bw.write(outToFile[i]);
					if (withEOL)
						bw.write(System.getProperty("line.separator"));					
				}

				// Closing BufferedWriter Stream
				bw.flush();
				bw.close();

			} catch (IOException ioe) {
				System.out.println("Exception occurred: writeArrayToFile()");
				ioe.printStackTrace();
				return false;
			}
		}
		return true;
	}

	// writes arrayList to a File.
	public boolean writeArrayListToFile(ArrayList<String>outToFile, boolean withEOL) {
		{
			try {
//				File file = new File(this.dateiName);

				// BufferedWriter writer give better performance
//				BufferedWriter bw = new BufferedWriter(new FileWriter(file, true));
				BufferedWriter bw = new BufferedWriter(new FileWriter(this.file));

				for (int i = 0; i < outToFile.size(); i++) {
					bw.write(outToFile.get(i));
					if (withEOL)
						bw.write(System.getProperty("line.separator"));					
				}

				// Closing BufferedWriter Stream
				bw.flush();
				bw.close();

			} catch (IOException ioe) {
				System.out.println("Exception occurred: writeArrayListToFile()");
				ioe.printStackTrace();
				return false;
			}
		}
		return true;
	}
	
	
	
	
	
	
	
	public static void saveFile(String content, File file) {
		try {
			FileWriter fileWriter = null;

			fileWriter = new FileWriter(file);
			fileWriter.write(content);
			fileWriter.close();
		} catch (IOException e) {
			System.out.println("DateiReadWrite SaveFile");
			e.printStackTrace();
		}
	}

	/*
	 * This logic is to create the file if the file is not already present
	 */
	// if(!file.exists()){
	// file.createNewFile();
	// }

	// Here true is to append the content to file
	// FileWriter fw = new FileWriter(file,true);

} // End Class DateiWriteData
