package application;

import java.io.IOException;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class MainPrim extends Application {

	private Stage primaryStage;
   private BorderPane rootLayout;
   
	
	@Override
	public void start(Stage primaryStage) {
		
		this.primaryStage = primaryStage;
      this.primaryStage.setTitle("Primzahlen");
      initRootLayout();
	}
	/**
    * Initializes the root layout.
    */
   private void initRootLayout() {
       try {
           // Load root layout from fxml file.
           FXMLLoader loader = new FXMLLoader();
           
           loader.setLocation(MainPrim.class.getResource("MainPrim.fxml"));
           rootLayout = (BorderPane) loader.load();

           // Give the controller access to the main app.
    		  MainPrimController controller = loader.getController();
    		  controller.setMainPrim(this);
//    		  controller.setPrimaryStage(primaryStage);
           // Show the scene containing the root layout.
           Scene scene = new Scene(rootLayout);
           this.primaryStage.setScene(scene);
           this.primaryStage.show();
       } catch (IOException e) {
           e.printStackTrace();
       }
   }	//Ende initRootLayout()
   
   
	
	
	public static void main(String[] args) {
		launch(args);
	}
}
