package application;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Optional;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;

/**
 * Description: Siehe Doc
 *
 * Project: PixelArt Class : MainController.java Package: application Created:
 * 26.02.2018 10:54:18
 *
 * @author Guenter Schmitz
 * @version 1.0.0
 **/

public class MainPrimController implements Initializable, Konstanten {

	// @FXML
	// private Pane anchorPane; // wird schon im FXML File gesetzt
	// private AnchorPane anchorPane; // wird schon im FXML File gesetzt

	// @FXML
	// private ScrollPane sp;

	// private MainPrim mainPrim;

	@FXML
	private TextArea txa_Radix2;

	@FXML
	private TextArea txa_Radix3;

	@FXML
	private TextArea txa_Radix4;

	@FXML
	private TextArea txa_Radix5;

	@FXML
	private TextArea txa_Radix6;

	@FXML
	private TextArea txa_Radix7;

	@FXML
	private TextArea txa_Radix8;

	@FXML
	private TextArea txa_Radix9;

	@FXML
	private TextArea txa_Radix10_1;

	@FXML
	private TextArea txa_Radix10_2;

	private Alert alert = new Alert(AlertType.ERROR);
	private ArrayList<String> primZahlenListe;
	
	private TextArea [] tlist = new TextArea [10];
	
	

	/*
	 * (non-Javadoc)
	 * 
	 * @see javafx.fxml.Initializable#initialize(java.net.URL,
	 * java.util.ResourceBundle)
	 */
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// System.out.println("I am here");
		// this.pixelModel = new PixelModel(this); // create Data Model
		// this.alert.setTitle("Fehlerhafte Eingabe");
		// this.alert.setHeaderText("Bitte korrigieren Sie Ihre Eingabe");
	}

	// Ab hier Einspruenge der MenueLeiste Datei
	/**
	 * Erstellt ein Pixelraster mittels Rectangels
	 *
	 * @param event
	 *           wenn in der Menueleiste Neu gedrückt wird
	 * @author G. Schmitz
	 * @since 1.0 (26.02.2018)
	 *
	 **/
	public void dateiNeu(ActionEvent event) {
		System.out.println("Datei Neu");

	}

	/**
	 * Datei öffnen
	 *
	 * @param event
	 *           wenn in der Menueleiste Öffnen gedrückt wird
	 * @author G. Schmitz
	 * @since 1.0 (26.02.2018)
	 *
	 **/
	public void dateiOeffnen(ActionEvent event) {
		System.out.println("Datei öffnen");

		File directory = new File(DEFAULTDIRECTORY);
		System.out.println("Defaultdirectory" + DEFAULTDIRECTORY);
		FileChooser fileChooser = new FileChooser();
		fileChooser.setInitialDirectory(directory);
		File selectedFile = fileChooser.showOpenDialog(null);

		if (selectedFile == null) {
			System.out.println("File selection canceled.");
			return;
		}
		// Hier wurde ein File ausgewählt, test ob ext. = txt
		System.out.println("File absolute Path: " + selectedFile.getAbsolutePath());

		String extension = "";

		int i = selectedFile.getName().lastIndexOf('.');
		if (i >= 0) {
			extension = selectedFile.getName().substring(i + 1);
		}

		if (!extension.equals("txt")) {
			System.out.println(TEXT_002);
			alert.setContentText(TEXT_002); // falsche Eingabe
			alert.showAndWait();
			return;
		}

		// Read StringArray from selected File
		DateiReadData readData = new DateiReadData(selectedFile.getAbsolutePath());
		primZahlenListe = readData.readAllLines();

		for (int p = 0; p < RADIXCOUNT; p++) {
			txa_Radix10_1.appendText(primZahlenListe.get(p));
			txa_Radix10_1.appendText("\n");
			txa_Radix10_2.appendText(primZahlenListe.get(p));
			txa_Radix10_2.appendText("\n");
		}
		ArrayList<ArrayList<String>> radizes;
		ArrayList<String> containerList;
		RadixChange radixChange = new RadixChange();

		radizes = radixChange.getRadixList(primZahlenListe, RADIXCOUNT);

		tlist [0] = txa_Radix2;
		tlist [1] = txa_Radix3;
		tlist [2] = txa_Radix4;
		tlist [3] = txa_Radix5;
		tlist [4] = txa_Radix6;
		tlist [5] = txa_Radix7;
		tlist [6] = txa_Radix8;
		tlist [7] = txa_Radix9;
		
		for (int loop =0; loop < 8; loop++){
			containerList = radizes.get(loop);	// first List (binaer)
			for (int p = 0; p < RADIXCOUNT; p++) {
				tlist [loop].appendText(containerList.get(p));
				tlist [loop].appendText("\n");
			}
		}

		System.out.println();
	}

	/**
	 * Datei speichern
	 *
	 * @param event
	 *           wenn in der Menueleiste Speichern gedrückt wird
	 * @author G. Schmitz
	 * @since 1.0 (26.02.2018)
	 *
	 **/
	public void dateiSpeichern(ActionEvent event) {
		System.out.println("Datei Speichern");

		File directory = new File(DEFAULTDIRECTORY);
		System.out.println("Defaultdirectory" + DEFAULTDIRECTORY);

		FileChooser fileChooser = new FileChooser();
		fileChooser.setInitialDirectory(directory);

		// Set extension filter
		FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("pixelart files (*.pixelart)",
				"*.pixelart");
		fileChooser.getExtensionFilters().add(extFilter);
		// Show save file dialog
		File file = fileChooser.showSaveDialog(null);
		// d.writeArrayListToFile(pal, true);
		// this.anchorPane.getChildren().clear(); // clear the Board
		// this.pixelModel.setBoardActive(false); // board not anymore in use

	}

	/**
	 * Beenden des Programmes
	 *
	 * @param event
	 *           wenn in der Menueleiste Beenden gedrückt wird
	 * @author G. Schmitz
	 * @since 1.0 (26.02.2018)
	 *
	 **/
	public void dateiBeenden(ActionEvent event) {
		System.out.println("Datei beenden");

		Platform.exit();
		System.exit(0);
		// this.anchorPane.getChildren().clear(); // clear the Board
	}

	/**
	 * Zeigt den Namen des Programms und den Autor
	 *
	 * @param event
	 *           wenn in der Menueleiste Beenden gedrückt wird
	 * @author G. Schmitz
	 * @since 1.0 (26.02.2018)
	 *
	 **/
	public void ueberPrimZahlen(ActionEvent event) {
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Information");
		alert.setHeaderText(null);
		alert.setContentText("Programm: Primzahlen" + "\n" + "Author      : Günter Schmitz");
		alert.showAndWait();
	}

	/**
	 * 
	 *
	 * @param
	 * @return
	 * @author G. Schmitz
	 * @since 1.0 (09.03.2018)
	 *
	 **/
	public void setMainPrim(MainPrim mainPrim) {
		// this.mainPrim = mainPrim;
	}

}
