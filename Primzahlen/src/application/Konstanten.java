package application;

import javafx.scene.paint.Color;

/**
 * Description:
 * Alle benoetigten Konstanten, z.B. max. Pixelanzahl
 * Project: PixelArt
 * Class  : Konstanten.java
 * Package: application
 * Created: 09.03.2018 08:34:06
 *
 * @author     Guenter Schmitz
 * @version    1.0.0
 **/
public interface Konstanten {

	// Pixelgroessen
	
	static final int RADIXCOUNT= 100;	// mindestens 3 Spalten
	static final int ROW_MINPIXEL= 3;	// mindestens 3 Spalten
	static final int COL_MINPIXEL= 3;	// mindestens 3 Zeilen
	static final int PICTURE_MAXPIXEL= 1024;	// max. 1024 Pixel
	static final int PIXEL_HEIGHT= 20;	// Hoehe
	static final int PIXEL_WIDTH= 20;	// Breite
	static final Color DEFAULTCOLOR=Color.WHITE;	// Breite
	static final String DEFAULTDIRECTORY ="D:\\DV_ORG\\Repositories\\Primzahlen\\Primzahlen";
	
	//Texte
	static final String TEXT_001 = "Anzahl Zeilen oder Spalten falsch! "
											+ "Mindestens 3 Zeilen und Spalten, höchstens 1024 Pixel (zB. 32x32)";
	static final String TEXT_002 = "Nur \"*.txt\" Dateien erlaubt(z.B. meinedatei.txt)";
	static final String TEXT_003 = "Datei muss mindestens 5 Zeilen enthalten.(siehe Dokumentation)";
	static final String TEXT_004 = "Dateikennung: \"*.pixelart\" fehlt";
	static final String TEXT_005 = "Formatfehler in Pixelart Datei. In Zeile 2 nur \"Spalte, Zeile\" erlaubt";
	static final String TEXT_006 = "kein aktives Pixelbild zum Speichern gefunden";
	static final String TEXT_007 = "Wenn Sie auf OK drücken wird das aktuelle Pixel Board gelöscht!";
	static final String TEXT_008 = "Kein Pixel Board vorhanden!";
}
